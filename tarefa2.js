let temperaturaFahrenheit = -20

function converter(temperaturaF) {
  let celsius = ((temperaturaF - 32) / 9) * 5
  return celsius.toFixed(1)
}

console.log(converter(temperaturaFahrenheit))
