let nota1 = 8
let nota2 = 5
let nota3 = 5

function verificarMedia(nota1, nota2, nota3) {
  somatorio = nota1 + nota2 + nota3
  if (somatorio < 0 || somatorio > 30) {
    return 'Erro: Valores fora do esperado.'
  }
  media = somatorio / 3

  if (media >= 6) {
    return 'Aprovado'
  } else {
    return 'Reprovado'
  }
}

console.log(verificarMedia(nota1, nota2, nota3))
