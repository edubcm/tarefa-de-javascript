import gods from './arquivo_exercicio_4.js'

// Q1 -> Prettier impede de deixar em uma linha só
// for (let god of gods) {console.log(god.name, god.features.length)}

// Q2

// for (let god of gods) {
//   if (god.roles.includes('Mid')) {
//     console.log(god)
//   } else {
//     continue
//   }
// }

//Q3

// function compare_pantheon(a, b) {
//   if (a.pantheon.toLowerCase() < b.pantheon.toLowerCase()) {
//     return -1
//   }
//   if (a.pantheon.toLowerCase() > b.pantheon.toLowerCase()) {
//     return 1
//   }
//   return 0
// }
// gods.sort(compare_pantheon)
// console.log(gods)

//Q4

// let arr = []
// for (let god of gods) {
//   arr.push(god.name + ' (' + god.class + ')')
// }
// console.log(arr)
