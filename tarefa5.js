arr = [12, 30, 23, 11, 20]

for (let number of arr) {
  if (number % 3 == 0 && number % 5 == 0) {
    console.log('fizzbuzz' + ' -> ' + number)
  } else if (number % 3 == 0) {
    console.log('buzz' + ' -> ' + number)
  } else if (number % 5 == 0) {
    console.log('fizz' + ' -> ' + number)
  } else {
    continue
  }
}
